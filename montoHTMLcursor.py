# HTMLcursor
# Adds the 'montoCursor' class to the element in which the cursor lies.

import montolib
from html.parser import HTMLParser

cursorClassName = 'montoCursor'

class MyHTMLParser(HTMLParser):

    def __init__(self):
        super(MyHTMLParser, self).__init__()
        self.allHTML = ''
        self.taglist  = []
        self.tagstack = []

    def buildmap(self, inputHTML):
        self.taglist  = []
        self.tagstack = []
        self.allHTML  = inputHTML
        self.feed(inputHTML)
        return self.taglist

    def handle_starttag(self, tag, attrs):
        tagtext = self.get_starttag_text()
        pos     = self.getpos_char()
        self.tagstack.append((tagtext, pos))

    def handle_endtag(self, tag):
        tagtext, startpos = self.tagstack.pop()

        endpos = self.getpos_char()
        endpos_later = self.allHTML.find('>', endpos)+1
        if (endpos_later == 0): endpos_later = endpos

        self.taglist.append((tagtext, startpos, endpos_later))



    def getpos_char(self):
        line, offset = self.getpos()
        linestart = self.findnth(self.allHTML, "\n", line-2) + 1
        return linestart + offset

    def findnth(self,haystack, needle, n):
        if (n == 0): return 0;
        parts= haystack.split(needle, n+1)
        if len(parts)<=n+1:
            return -1
        return len(haystack)-len(parts[-1])-len(needle)


# def ppprint(thing):
#     ### FOR DEBUGGING. REMOVE THIS LATER ###
#     import pprint
#     pp = pprint.PrettyPrinter(indent=4)
#     pp.pprint(thing)

def between(x, lesser, greater):
    if (x <= lesser):  return False
    if (x >= greater): return False
    return True

def addCursorClass(tagtext):
    start, end = tagtext, ''

    while (start[-1] in ('>', '/')):
        end = start[-1] + end
        start = start[:-1]

    return start + ' class="' + cursorClassName + '"' + end


def markCursor (version):

    selections = version['selections']
    allHTML    = version['contents']

    parser = MyHTMLParser()
    tagmap = parser.buildmap(allHTML)

    #todo this will break on multiple selections, because the character indexes will lose their meaning on a string replace
    if len(selections) > 0:
        selection = selections[0]
        begin = selection['begin']
        tagsImIn = [(tag, start, end) for (tag, start, end) in tagmap if between(begin, start, end)]
        tagsImIn = sorted(tagsImIn, key=lambda a: a[1], reverse=True)
        if (len(tagsImIn)>0):
            thistag = tagsImIn[0]
            tagtext = thistag[0]
            newtagtext = addCursorClass(tagtext)
            allHTML = allHTML[:thistag[1]] + newtagtext + allHTML[thistag[1]+len(tagtext):]


    return [('HTMLcursor', 'html', allHTML, True)]


montolib.server (markCursor, 'html')

